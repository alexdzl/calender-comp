﻿using System;

namespace CalendarDayTests
{
    public class CalendarDay
    {
        public DateTime Day { get; set; }
        public bool IsHoliday { get; set; }
    }
}